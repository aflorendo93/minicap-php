<?php

require "connection.php";

session_start();

$firstTask = $_POST['firstTask'];
$secondTask = $_POST['secondTask'];
$thirdTask = $_POST['thirdTask'];
$status = 0;

$date = $_SESSION['date'];
$find_dateId_query = "SELECT id FROM dates WHERE taskDate = '$date'";
$find_dateId = mysqli_query($conn, $find_dateId_query);

foreach ($find_dateId as $indivDateId){

    $_SESSION['dateId'] = $indivDateId['id'];
};

$dateId = $_SESSION['dateId']; 

$email = $_SESSION['mail'];
$user_query = "SELECT id FROM users WHERE email = '$email'";
$user = mysqli_query($conn, $user_query);

foreach($user as $indivUser){

    $_SESSION['userId'] = $indivUser['id'];
}

$userId = $_SESSION['userId']; 


if($firstTask && !$secondTask && !$thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if(!$firstTask && $secondTask && !$thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$secondTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if(!$firstTask && !$secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if($firstTask && $secondTask && !$thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId), ('$secondTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if($firstTask && !$secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId), ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if(!$firstTask && $secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$secondTask', $status, $userId, $dateId), ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if($firstTask && $secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId), ('$secondTask', $status, $userId, $dateId), ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else {
    echo "Please follow instructions";
}

?>