<?php

require "connection.php";

session_start();

$firstTask = $_POST['firstTask'];
$secondTask = $_POST['secondTask'];
$thirdTask = $_POST['thirdTask'];
$userId  = $_POST['userId'];
$dateId = $_POST['dateId'];
$status = 0;


if($firstTask && !$secondTask && !$thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if(!$firstTask && $secondTask && !$thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$secondTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if(!$firstTask && !$secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if($firstTask && $secondTask && !$thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId), ('$secondTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if($firstTask && !$secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId), ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if(!$firstTask && $secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$secondTask', $status, $userId, $dateId), ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else if($firstTask && $secondTask && $thirdTask){

    $insert_tasks_query = "INSERT INTO tasks (task, status, user_id, date_id) VALUES ('$firstTask', $status, $userId, $dateId), ('$secondTask', $status, $userId, $dateId), ('$thirdTask', $status, $userId, $dateId)";

    $insert_tasks = mysqli_query($conn, $insert_tasks_query);

    header("Location: ../todolist.php");

}else {
    echo "Please follow instructions";
}

?>