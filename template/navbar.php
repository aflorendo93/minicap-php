
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="../index.php"><img src="../assets/images/logo.png" alt=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
        <?php

        if(!isset($_SESSION['user'])){

        ?>
        <li class="nav-item">
          <a class="nav-link" href="../login.php">Login</a>
        </li>      
        <li class="nav-item">
          <a class="nav-link" href="../register.php">Register</a>
        </li>
        <?php
        } else{
        ?>
        <li class="nav-item active">
          <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../todolist.php">To Do Lists</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../add-date.php">Add To Do List</a>
        </li>
        <?php
        };
        ?>
      </ul>
  </div>
</nav>
