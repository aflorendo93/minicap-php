<?php

require "template/template.php";

function getContent(){

require "controllers/connection.php"

?>
<!-- <div class="container"> -->
        <div class="col-lg-12">
            <h1 class="text-center display-3 my-5">MY TO DO LISTS</h1>
            <div class="row">

                <?php

                    $userId = $_SESSION['user']['id'];

                    $dates_query = "SELECT DISTINCT dates.id, dates.taskDate, dates.taskDay FROM dates JOIN tasks ON (dates.id = tasks.date_id) WHERE user_id = $userId ORDER BY taskDate ASC";

                    $dates = mysqli_query($conn, $dates_query);

                    foreach($dates as $indivDate){

                ?>

                <div class="col-lg-2 ">
                    <div class="card">
                        <h3><?php echo $indivDate['taskDay']?></h3>
                        <h4><?php echo date("F j, Y", strtotime($indivDate['taskDate']))?></h4>
                                <?php
                                    $tasks_query = "SELECT tasks.id, dates.taskDate, dates.taskDay, tasks.task, tasks.status FROM tasks JOIN dates ON (dates.id = tasks.date_id) WHERE tasks.user_id = $userId";

                                    $tasks = mysqli_query($conn, $tasks_query);

                                    foreach($tasks as $indivTask){
                                    if($indivDate['taskDate'] === $indivTask['taskDate']){
                                ?> 
                                    <span class=<?php echo $indivTask['status'] === '0'? "" : " mark"?>><?php echo $indivTask['task']?>
                                    <?php
                                    if($indivTask['status'] === '0'){
                                    ?>
                                    <a href="controllers/process_change_status.php?status=<?php echo $indivTask['status']?>&id=<?php echo $indivTask['id']?>&user_id=<?php echo $userId ?>" class="btn btn-warning">Not Done</a>
                                    <?php
                                    }else{
                                    ?>
                                    <a href="controllers/process_change_status.php?status=<?php echo $indivTask['status']?>&id=<?php echo $indivTask['id']?>&user_id=<?php echo $userId ?>" class="btn btn-success">Done</a>
                                    <?php
                                    };
                                    ?>
                                    <a href="edit-task.php?taskId=<?php echo $indivTask['id'] ?>&taskInfo=<?php echo $indivTask['task'] ?>"><i class="far fa-edit btn-outline-warning"></i></a>
                                    <a href="controllers/process_delete_task.php?id=<?php echo $indivTask['id'] ?>"><<i class="far fa-trash-alt btn-outline-danger"></i></a>
                                    </span><br>
                                <?php
                                    };
                                };
                                                                 
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <a href="add-more-task.php?datesId=<?php echo $indivDate['id']?>&userId=<?php echo $userId ?>" class="btn btn-info">Add More</a>
                        <a href="controllers/process_delete_list.php?taskDate=<?php echo $indivDate['taskDate'] ?>" class="btn btn-danger">Delete List</a>
                    </div>
                </div>
                <?php
                    };

                ?>
        </div>
<!-- </div> -->

<?php
};

?>
