<?php

require "template/landing.php";
    function getLanding(){

?>
<div class="container">
    <div class="my-5">
        <div class="">
            <h3 class="">todo list without complexity</h3>
            <div class="img-fluid">
                <img src="assets/images/logo-bg.png" alt="" class="">
            </div>
            
            <?php
                if(!isset($_SESSION['user'])){
            ?>
            <div class="offset-1 mt-3">
            <a href="register.php"><button class="btn btn-danger pt-4"><h3 class="text-light">Sign Up for Free</h3></button></a>
                <blockquote class="blockquote mt-3">Already have an account? <u class="text-danger"><a href="login.php" class="text-danger">Login here.</a></u></blockquote>
            </div>
                
            <?php
                }else {
            ?>

            <div class="offset-1 mt-3">
                <h1>WELCOME<?php echo " ". $_SESSION['user']['firstName']. " " . $_SESSION['user']['lastName'] ?></h1>
                <a href="todolist.php"><button class="btn btn-danger pt-4"><h3 class="text-light">Go! do your lists</h3></button></a>
            </div>     

            <?php
            };
            ?>
        
        
        </div>
            
    </div>
    
</div>


<?php

};
?>