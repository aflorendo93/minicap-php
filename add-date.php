<?php
require "template/template.php";

function getContent(){
?>

    <div class="d-flex align-items-center flex-column mt-5">
        <h1>Hi! please select date for the task list you want to create</h1>
        <div class="d-flex justify-content-center">
            <form action="controllers/process_add_date.php" method="post">
                <div class="form-group">
                    <label for="taskDate">Date :</label>
                    <input type="search" id="datepicker" name="taskDate" autocomplete="off">
                    <input type="hidden" id="day" name="taskDay">
                    <button class="btn btn-info" type="submit">Add List</button>
                </div>
            </form>
        </div>
    </div>


<?php
};

?>
