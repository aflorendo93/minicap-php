<?php

require "template/template.php";

function getContent() {


?>
    <h1>Edit Task</h1>
    <div class="d-flex align-items-center diplay-flex-center flex-column">
        <form action="controllers/process_edit_task.php" method="POST" enctype="multipart/form-data">
        <?php
            $taskId = $_GET['taskId'];
            $taskInfo = $_GET['taskInfo'];
        ?>
            <div class="form-group">
                <label for="taskEdit">Task to be edited:</label>
                <input type="text" name="taskEdit" class="form-control" value ="<?php echo $taskInfo = $_GET['taskInfo'] ?>" disabled>
            </div>
            <div class="form-group">
                <label for="newTask">Edit Task:</label>
                <input type="text" name="newTask" class="form-control">
            </div>
            <input type="hidden" name="taskId" value="<?php echo $taskId ?>">
            <button class="btn btn-info" type="submit">Edit-Task</button>
        </form>
    </div>   


<?php
};

?>